package parejas;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by TheL0w3R on 30/10/2018.
 * All Rights Reserved.
 */
public class GameManager {

    private static GameManager instance;

    private ArrayList<Card> cards;
    private boolean canConsume;
    private Card card1;
    private Card card2;
    private int tries;
    private int pairs;

    private JLabel triesLabel;

    public GameManager() {
        instance = this;

        cards = new ArrayList<>();
        cards.add(new Card(new ImageIcon(getClass().getResource("/res/copas.png")), CardValue.COPA));
        cards.add(new Card(new ImageIcon(getClass().getResource("/res/copas.png")), CardValue.COPA));
        cards.add(new Card(new ImageIcon(getClass().getResource("/res/bastos.png")), CardValue.BASTO));
        cards.add(new Card(new ImageIcon(getClass().getResource("/res/bastos.png")), CardValue.BASTO));
        cards.add(new Card(new ImageIcon(getClass().getResource("/res/espadas.png")), CardValue.ESPADA));
        cards.add(new Card(new ImageIcon(getClass().getResource("/res/espadas.png")), CardValue.ESPADA));
        cards.add(new Card(new ImageIcon(getClass().getResource("/res/oros.png")), CardValue.ORO));
        cards.add(new Card(new ImageIcon(getClass().getResource("/res/oros.png")), CardValue.ORO));

        tries = 0;
        pairs = 0;

        Collections.shuffle(cards);
        canConsume = true;
    }

    public static GameManager getInstance() {
        return instance;
    }

    public void setTriesLabel(JLabel label) {
        this.triesLabel = label;
    }

    public Card consumeCard() {
        Card c = null;
        if(canConsume) {
            c = cards.get(0);
            cards.remove(0);
            if (cards.size() <= 0)
                canConsume = false;
        }
        return c;
    }

    public boolean compareCards() {
        if(card1 == null | card2 == null)
            return false;
        else {
            boolean win = card1.getValue() == card2.getValue();
            if(!win) {
                tries++;
                triesLabel.setText("Intentos fallidos: " + tries);
            } else {
                pairs++;
            }
            return win;
        }
    }

    public boolean playCard(Card c) {
        boolean canPlay = false;

        if(!c.isTurned()) {
            if(card1 == null) {
                canPlay = true;
                card1 = c;
            } else if(card2 == null) {
                canPlay = true;
                card2 = c;
            }
        } else {
            if(card1 == c)
                card1 = null;
            else if(card2 == c)
                card2 = null;
            canPlay = true;
        }

        return canPlay;
    }

    public Card getCard1() {
        return card1;
    }

    public Card getCard2() {
        return card2;
    }

    public void reset() {
        card1 = null;
        card2 = null;
    }

    public int getPairs() {
        return pairs;
    }

    public int getTries() {
        return tries;
    }
}
