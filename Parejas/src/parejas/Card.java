package parejas;

import javax.swing.*;

/**
 * Created by TheL0w3R on 30/10/2018.
 * All Rights Reserved.
 */
public class Card {

    public static ImageIcon BACK = new ImageIcon(Card.class.getResource("/res/back.png"));

    private ImageIcon icon;
    private CardValue value;
    private boolean isTurned;
    private JToggleButton self;

    public Card(ImageIcon icon, CardValue value) {
        this.icon = icon;
        this.value = value;
        isTurned = false;
        self = null;
    }

    public ImageIcon getIcon() {
        return icon;
    }

    public void setIcon(ImageIcon icon) {
        this.icon = icon;
    }

    public CardValue getValue() {
        return value;
    }

    public void setValue(CardValue value) {
        this.value = value;
    }

    public void toggleCard() {
        this.isTurned = !this.isTurned;
    }

    public boolean isTurned() {
        return this.isTurned;
    }

    public void setButton(JToggleButton button) {
        this.self = button;
    }

    public void disableCard() {
        self.setIcon(null);
        self.setEnabled(false);
    }
}
