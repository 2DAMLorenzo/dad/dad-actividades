package parejas;

import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created by TheL0w3R on 30/10/2018.
 * All Rights Reserved.
 */
public class CardAction implements ItemListener {

    private Card card;
    private GameManager gameManager;

    public CardAction(Card card, JToggleButton button) {
        this.card = card;
        card.setButton(button);
        gameManager = GameManager.getInstance();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if(gameManager.playCard(card)) {
            if(card.isTurned())
                ((JToggleButton) e.getSource()).setIcon(Card.BACK);
            else
                ((JToggleButton) e.getSource()).setIcon(card.getIcon());

            card.toggleCard();

            if(gameManager.compareCards()) {
                gameManager.getCard1().disableCard();
                gameManager.getCard2().disableCard();
                gameManager.reset();
                if(gameManager.getPairs() >= 4) {
                    JOptionPane.showMessageDialog(null, "Has ganado la partida con " + gameManager.getTries() + " intentos fallidos!", "Victoria!", JOptionPane.PLAIN_MESSAGE);
                }
            }
        }
    }
}
