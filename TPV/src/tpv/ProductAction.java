package tpv;

import javafx.scene.control.Tab;
import tpv.TableManager;
import tpv.view.MainForm;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by TheL0w3R on 30/10/2018.
 * All Rights Reserved.
 */
public class ProductAction implements ActionListener {

    private TableManager tableManager;
    private MainForm formInstance;

    public ProductAction(MainForm formInstance) {
        tableManager = TableManager.getInstance();
        this.formInstance = formInstance;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String value = JOptionPane.showInputDialog("Introduzca la cantidad de productos:");
        try {
            int amount = Integer.parseInt(value);
            String[] data = ((JButton)e.getSource()).getText().split("-");
            String name = data[0].trim();
            double price = Double.parseDouble(data[1].trim().substring(0, data[1].trim().indexOf("€")));
            tableManager.addProduct(name, amount, price);
            formInstance.updateData();
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(formInstance, "La cantidad de productos debe ser un número entero positivo válido.", "Error de entrada", JOptionPane.ERROR_MESSAGE);
        }
    }
}
