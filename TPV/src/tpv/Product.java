package tpv;

/**
 * Created by TheL0w3R on 30/10/2018.
 * All Rights Reserved.
 */
public class Product {
    private String name;
    private int amount;
    private double subtotal;

    public Product(String name, int amount, double subtotal) {
        this.name = name;
        this.amount = amount;
        this.subtotal = subtotal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public String[] getRaw() {
        return new String[]{name, String.valueOf(amount), String.valueOf(Utils.round(subtotal)) + "€"};
    }
}
