package tpv;

import tpv.view.LoginForm;

import javax.swing.*;

/**
 * Created by thel0w3r on 29/10/2018.
 * All Rights Reserved.
 */
public class Main {
    public static void main(String[] args) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            try {
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (Exception ignored) {
            }
        }
        new TableManager();
        new LoginForm();
    }

}
