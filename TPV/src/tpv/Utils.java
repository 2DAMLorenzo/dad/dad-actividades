package tpv;

/**
 * Created by TheL0w3R on 30/10/2018.
 * All Rights Reserved.
 */
public class Utils {

    public static double round(double value) {
        return (double)Math.round(value * 100D) / 100D;
    }

}
