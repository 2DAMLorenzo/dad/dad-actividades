package tpv;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by TheL0w3R on 30/10/2018.
 * All Rights Reserved.
 */
public class TableManager {

    private static TableManager instance;

    private ArrayList<Product> products;

    public TableManager() {
        instance = this;
        products = new ArrayList<>();
    }

    public static TableManager getInstance() {
        return instance;
    }

    public void addProduct(String name, int amount, double price) {
        products.add(new Product(name, amount, (double)amount*price));
    }

    public Product[] getProducts() {
        return products.toArray(new Product[]{});
    }

    public double getTotal() {
        double total = 0D;
        for(Product p : products)
            total += p.getSubtotal();

        return Utils.round(total);
    }

    public void clear() {
        products.clear();
    }
}
